<?php if( have_rows('cta') ):
	while ( have_rows('cta') ) : the_row();
		
		switch ( get_row_layout()) {

			// Alpha ~ Regular text block
			case 'cta_alpha':
				get_template_part('/templates/template-parts/flexible-components/text-block');
			break;
			
			// Copy Image Block
			case 'grid_content':
				get_template_part('/templates/template-parts/flexible-components/grid-content');
			break;

			// Buttons Block
			case 'buttons':
				get_template_part('/templates/template-parts/flexible-components/buttons');
			break;

			// Video Block
			case 'full_width_media':
				get_template_part('/templates/template-parts/flexible-components/media');
			break;

			// Block Title
			case 'section_title':
				get_template_part('/templates/template-parts/flexible-components/block-title');
			break;

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>