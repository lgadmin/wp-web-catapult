<div class="utility-bar">
  <div class="">
    <div class="left">
      <?php echo do_shortcode('[lg-social-media]'); ?>
      <div class="header-address">
        <i class="fa fa-map-marker" aria-hidden="true"></i> <span><?php echo do_shortcode('[lg-address1]'); ?> <?php echo do_shortcode('[lg-address2]'); ?>, <?php echo do_shortcode('[lg-city]'); ?>, <?php echo do_shortcode('[lg-province]'); ?></span>
      </div>
    </div>
    <div class="right">
      <div>
        <a href="mailto:<?php echo do_shortcode('[lg-email]'); ?>"><i class="fa fa-envelope" aria-hidden="true"></i> <span><?php echo do_shortcode('[lg-email]'); ?></span></a>
      </div>
      <div>
        <a href="tel:<?php echo do_shortcode('[lg-phone-main]'); ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span><?php echo format_phone(do_shortcode('[lg-phone-main]')); ?></span></a>
      </div>
    </div>
  </div>
</div>