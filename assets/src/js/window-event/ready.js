// Windows Ready Handler

(function($) {

    $(document).ready(function(){

      	//Mobile Toggle
        $('.mobile-toggle').on('click', function(){
      		$('.navbar-collapse').fadeToggle();
      	});

        $('.dropdown-toggle').on('click', function(e){
          e.preventDefault();
        });

        $('.dropdown-toggle').closest('li').on('mouseover', function(e){
          $(this).addClass('open');
        });

        $('.dropdown-toggle').closest('li').on('mouseleave', function(e){
          $(this).removeClass('open');
        });

        $('.feature-slider').slick({
          autoplay: true,
          autoplaySpeed: 6000
        });

      //If Mega Menu
      /*$('.mobile-toggle').on('click', function(){
        $('.mega-menu-toggle').click();
      });*/
        
    });

}(jQuery));