<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<div id="primary">
		<main id="content" role="main" class="site-content">
			<div class="body-copy">
				
				<div class="jumbotron mt-5">
					<h1 class="display-4"><?php single_post_title(); ?></h1>
				</div>

				<?php get_template_part( 'templates/template-parts/content/content-loop'); ?>

			</div>
			<?php get_sidebar(); ?>
		</main>
	</div>

<?php get_footer(); ?>