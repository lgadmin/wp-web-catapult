<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		
		<div id="site-footer" class="bg-secondary text-white clearfix pt-4 pb-3 px-3">
			<div class="container">
				<div class="site-footer-alpha"><?php dynamic_sidebar('footer-alpha'); ?></div>
				<div class="site-footer-bravo"><?php dynamic_sidebar('footer-bravo'); ?></div>
				<div class="site-footer-charlie"><?php dynamic_sidebar('footer-charlie'); ?></div>
			</div>
		</div>

		<div id="site-legal" class="pt-0 pb-4 px-3 clearfix">
			<div class="container">
				<div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
			</div>
		</div>

	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
